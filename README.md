# Livre blanc non technique

Traduction en français du [non-technical whitepaper](https://github.com/nebulasio/nebdocs/blob/master/docs/go-nebulas/papers/nebulas-white-paper.md) de [Nebulas](https://nebulas.io).